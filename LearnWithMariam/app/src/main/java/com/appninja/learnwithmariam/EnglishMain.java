package com.appninja.learnwithmariam;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class EnglishMain extends AppCompatActivity {
 private ImageView a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,back;
 private Intent intent;
 private AdView adView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_english_main);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        adView = findViewById(R.id.english_main_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        back=(ImageView)findViewById(R.id.back);
        a=(ImageView)findViewById(R.id.a);
        b=(ImageView)findViewById(R.id.b);
        c=(ImageView)findViewById(R.id.c);
        d=(ImageView)findViewById(R.id.d);
        e=(ImageView)findViewById(R.id.e);
        f=(ImageView)findViewById(R.id.f);
        g=(ImageView)findViewById(R.id.g);
        h=(ImageView)findViewById(R.id.h);
        i=(ImageView)findViewById(R.id.i);
        j=(ImageView)findViewById(R.id.j);
        k=(ImageView)findViewById(R.id.k);
        l=(ImageView)findViewById(R.id.l);
        m=(ImageView)findViewById(R.id.m);
        n=(ImageView)findViewById(R.id.n);
        o=(ImageView)findViewById(R.id.o);
        p=(ImageView)findViewById(R.id.p);
        q=(ImageView)findViewById(R.id.q);
        r=(ImageView)findViewById(R.id.r);
        s=(ImageView)findViewById(R.id.s);
        t=(ImageView)findViewById(R.id.t);
        u=(ImageView)findViewById(R.id.u);
        v=(ImageView)findViewById(R.id.v);
        w=(ImageView)findViewById(R.id.w);
        x=(ImageView)findViewById(R.id.x);
        y=(ImageView)findViewById(R.id.y);
        z=(ImageView)findViewById(R.id.z);



        intent=new Intent(getApplication(),English_container.class);

        back.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       //EnglishMain.this.onBackPressed();

        intent=new Intent(getApplication(),Home_activity.class);
       startActivity(intent);
       //EnglishMain.this.finish();

    }
});
a.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {



        English_Resource.page_number=0;
     startActivity(intent);

     ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.a);

        //     constraintLayout.setVisibility(GONE);
    }
});

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
English_Resource.page_number=1;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.b);

                //     constraintLayout.setVisibility(GONE);
            }
        });


        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=2;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.c);

                //     constraintLayout.setVisibility(GONE);
            }
        });


        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=3;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.d);

                //     constraintLayout.setVisibility(GONE);
            }
        });
        e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=4;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.e);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=5;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.f);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=6;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.g);
                //     constraintLayout.setVisibility(GONE);
            }
        });
        h.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=7;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.h);

                //     constraintLayout.setVisibility(GONE);
            }
        });
        i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=8;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.i);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        j.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=9;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.j);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=10;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.k);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=11;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.l);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=12;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.m);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        n.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=13;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.n);

                //     constraintLayout.setVisibility(GONE);
            }
        });


        o.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=14;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.o);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=15;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.p);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        q.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=16;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.q);
                //     constraintLayout.setVisibility(GONE);
            }
        });

        r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=17;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.r);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=18;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.s);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=19;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.t);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        u.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=20;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.u);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=21;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.v);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        w.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=22;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.w);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=23;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.x);

                //     constraintLayout.setVisibility(GONE);
            }
        });

        y.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=24;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.y);

                //     constraintLayout.setVisibility(GONE);
            }
        });


        z.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                English_Resource.page_number=25;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.z);

                //     constraintLayout.setVisibility(GONE);
            }
        });

    startZoomInAnimation();
    }

    @Override
    public void finish() {
        super.finish();

    }

    public void startZoomInAnimation() {


        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);

        a.startAnimation(animation);
       // a.startAnimation(rotate);


        b.startAnimation(animation);
        c.startAnimation(animation);
        d.startAnimation(animation);
        e.startAnimation(animation);
        f.startAnimation(animation);
        g.startAnimation(animation);
        h.startAnimation(animation);
        i.startAnimation(animation);
        j.startAnimation(animation);
        k.startAnimation(animation);
        l.startAnimation(animation);
        m.startAnimation(animation);
        n.startAnimation(animation);
        o.startAnimation(animation);
        p.startAnimation(animation);
        q.startAnimation(animation);
        r.startAnimation(animation);
        s.startAnimation(animation);
        t.startAnimation(animation);
        u.startAnimation(animation);
        v.startAnimation(animation);
        w.startAnimation(animation);
        x.startAnimation(animation);
        y.startAnimation(animation);
        z.startAnimation(animation);

    }
}