package com.appninja.learnwithmariam;

/**
 * Created by SHOAIB on 11/29/2017.
 */

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

// it is the class for viewPager that will handle the viewPager
public class English_CustomPageAdapter extends PagerAdapter {
    //creating inilial refernce and Arrays that we inilize after
    private Context context;
    int images_upper[];
    int image_left[];
    int image_right[];
    int image_down[];
    private int increament;
    private InterstitialAd mInterstitialAd;

    // int[] sounds1 = {R.raw.a,R.raw.b,R.raw.c,R.raw.d,R.raw.e,R.raw.f,R.raw.g,R.raw.h,R.raw.i,R.raw.j,R.raw.k,R.raw.l,R.raw.m,R.raw.n,R.raw.o,R.raw.p,R.raw.q,R.raw.r,R.raw.s,R.raw.t,R.raw.u,R.raw.v,R.raw.w,R.raw.x,R.raw.y,R.raw.z};


    int images_bottom[];
    //Inflater refernce
    LayoutInflater layoutInflater;


// this is the constructer that will inlize given reference when once it is called

    public English_CustomPageAdapter(Context context) {
        //context handles whole envieroment of app
        this.context = context;
        //getting array of upper images
        MobileAds.initialize(context, "ca-app-pub-2173820520715946~4900274980");
        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        this.images_upper = English_Resource.images_upper;
        this.images_bottom = English_Resource.images_bottom;
        this.image_left = English_Resource.image_left;
        this.image_right = English_Resource.images_right;
        this.image_down = English_Resource.image_down;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //Builtin method that return to adapter total number of pages
    @Override
    public int getCount() {
        return images_upper.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ConstraintLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {


        View itemView = layoutInflater.inflate(R.layout.fragment_english_detail, container, false);

        ImageView back_button = (ImageView) itemView.findViewById(R.id.english_back);
        final ImageView uper_view = (ImageView) itemView.findViewById(R.id.upper_image);
        final ImageView left_image = (ImageView) itemView.findViewById(R.id.left_image);
        final ImageView right_image = (ImageView) itemView.findViewById(R.id.right_image);
        final ImageView down_image = (ImageView) itemView.findViewById(R.id.down_image);
        final ImageView bottom_image = (ImageView) itemView.findViewById(R.id.bottom_image);
        uper_view.setClickable(true);
        uper_view.bringToFront();
        increament++;
        add_show();

        uper_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation rotate = AnimationUtils.loadAnimation(view.getContext(), R.anim.zoom_in);
                uper_view.startAnimation(rotate);
                Log.e("Clicked", "Upper image is clicked");
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(view.getContext(), English_sound_resources.sound_upper[position]);


            }
        });


        left_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation rotate = AnimationUtils.loadAnimation(view.getContext(), R.anim.zoom_in);
                left_image.startAnimation(rotate);
                Log.e("Clicked", "Left image is clicked");
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(view.getContext(), English_sound_resources.sound_left[position]);


            }
        });

        down_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation rotate = AnimationUtils.loadAnimation(view.getContext(), R.anim.zoom_in);
                down_image.startAnimation(rotate);
                Log.e("Clicked", "Down image is clicked");
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(view.getContext(), English_sound_resources.sound_down[position]);


            }
        });

        right_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation rotate = AnimationUtils.loadAnimation(view.getContext(), R.anim.zoom_in);
                right_image.startAnimation(rotate);
                Log.e("Clicked", "Right image is clicked");
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(view.getContext(), English_sound_resources.sound_right[position]);


            }
        });


        left_image.setImageResource(image_left[position]);
        right_image.setImageResource(image_right[position]);

        down_image.setImageResource(image_down[position]);


        //setting resource of backbuttton
        uper_view.setImageResource(images_upper[position]);
        bottom_image.setImageResource(images_bottom[position]);
        back_button.setImageResource(English_Resource.back_button);
        //add view to the conatainer

        container.addView(itemView);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), EnglishMain.class);
                context.startActivity(intent);
                //  context.startActivity(new Intent(context,English_Resource.class));
            }
        });
        //listening to image click
//        lower_view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
//
//            }
//        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((ConstraintLayout) object);

    }

    public void btnClicked(View v) {
        Animation rotate = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        v.setAnimation(rotate);
    }

    public void add_show() {
        if (increament >= 10) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();

            } else {
                mInterstitialAd = new InterstitialAd(context);
                mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                mInterstitialAd.show();
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
            mInterstitialAd = new InterstitialAd(context);
            mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            increament = 0;

        }


    }
}