package com.appninja.learnwithmariam;

/**
 * Created by SHOAIB on 11/28/2017.
 */

public class English_Resource {
    /***
     *
     * two images are in Viewpager
     * so here defined both images here
     *
     * ****/

    static int images_upper[] = {
            R.drawable.apple,
            R.drawable.bat,
            R.drawable.camel,
            R.drawable.dolfin,
            R.drawable.elephant,
            R.drawable.fish,
            R.drawable.goat,
            R.drawable.hourse,
            R.drawable.igloo,
            R.drawable.jug,
            R.drawable.kite,
            R.drawable.lemon,
            R.drawable.monkey,
            R.drawable.notebook,
            R.drawable.orange,
            R.drawable.pen,
            R.drawable.quail,
            R.drawable.robot,
            R.drawable.sun,
            R.drawable.turtle,
            R.drawable.umbrella,
            R.drawable.varn,
            R.drawable.watermelon,
            R.drawable.xlophone,
            R.drawable.yoyo,
            R.drawable.zip};

    static int image_left[] = {
            R.drawable.ambulance,
            R.drawable.ball,
            R.drawable.car,
            R.drawable.doll,

            R.drawable.eagle,
            R.drawable.fan,
            R.drawable.gorilla,
            R.drawable.hat,
            R.drawable.ice_cream,
            R.drawable.jacket,
            R.drawable.kangrro,
            R.drawable.leaf,
            R.drawable.mango,
            R.drawable.nest,
            R.drawable.octpus,
            R.drawable.panda,
            R.drawable.quilt,
            R.drawable.rabbit,
            R.drawable.sheep,
            R.drawable.train,
            R.drawable.unicom,
            R.drawable.van,
            R.drawable.watch,
            R.drawable.xfish,
            R.drawable.yak,
            R.drawable.zebra};


    static int images_right[] = {
            R.drawable.ant,
            R.drawable.birds,
            R.drawable.cat,
            R.drawable.door,
            R.drawable.eggs,
            R.drawable.flower,
            R.drawable.graff,
            R.drawable.helecopter,
            R.drawable.ink,
            R.drawable.jam,
            R.drawable.key,
            R.drawable.lamp,

            R.drawable.moon,
            R.drawable.net,
            R.drawable.onion,
            R.drawable.pea_cock,
            R.drawable.question,
            R.drawable.rocket,
            R.drawable.shoes,
            R.drawable.tree,
            R.drawable.urn,
            R.drawable.viillan,
            R.drawable.wel_fish,
            R.drawable.xmastree,
            R.drawable.yart,
            R.drawable.zero,
    };
    static int image_down[] = {
            R.drawable.alarm,
            R.drawable.bus,
            R.drawable.cow,
            R.drawable.drum,
            R.drawable.eye,
            R.drawable.frog,
            R.drawable.guittar,
            R.drawable.house,
            R.drawable.iron,
            R.drawable.jar,
            R.drawable.king,

            R.drawable.lion,
            R.drawable.mouse,
            R.drawable.nut,
            R.drawable.owl,
            R.drawable.parrot,
            R.drawable.queen,
            R.drawable.rose,
            R.drawable.ship,
            R.drawable.tomato,
            R.drawable.utensils,
            R.drawable.vulture,
            R.drawable.window,
            R.drawable.xray,
            R.drawable.yarn,
            R.drawable.zigzag};

    static int images_bottom[] = {
            R.drawable.aaa,
            R.drawable.bbb,
            R.drawable.ccc,
            R.drawable.ddd,
            R.drawable.eee,
            R.drawable.fff,
            R.drawable.ggg,
            R.drawable.hhh,
            R.drawable.iii,
            R.drawable.jjj,
            R.drawable.kkk,
            R.drawable.lll,
            R.drawable.mmm,
            R.drawable.nnn,
            R.drawable.ooo,
            R.drawable.ppp,
            R.drawable.qqq,
            R.drawable.rrr,
            R.drawable.sss,
            R.drawable.ttt,
            R.drawable.uuu,
            R.drawable.vvv,
            R.drawable.www,
            R.drawable.xxx,
            R.drawable.yyy,
            R.drawable.zzz};


    public static int page_number;
// it returns back_button id in the for of int
    public static int back_button = R.drawable.back_button;
}