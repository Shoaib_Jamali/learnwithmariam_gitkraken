package com.appninja.learnwithmariam;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 *it is Fraagment for our English Alphabate
 * */
public class English_detail extends Fragment {
    // need ViewPager and Adapter reference
    private ViewPager viewPager;
    private English_CustomPageAdapter englishCustomPageAdapter;
    private ImageView back,background;
    //int[] sounds1 = {R.raw.a,R.raw.b,R.raw.c,R.raw.d,R.raw.e,R.raw.f,R.raw.g,R.raw.h,R.raw.i,R.raw.j,R.raw.k,R.raw.l,R.raw.m,R.raw.n,R.raw.o,R.raw.p,R.raw.q,R.raw.r,R.raw.s,R.raw.t,R.raw.u,R.raw.v,R.raw.w,R.raw.x,R.raw.y,R.raw.z};
    ImageView upperImage;
    int current_Page;



    // creating empty constructor that we can creates this class reference to call it bcz it does not contains default constructor
    public English_detail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_english_detail, container, false);
//find viewsbyId for ViewPager


        back = view.findViewById(R.id.english_back);
        background = view.findViewById(R.id.english_back);
        viewPager = (ViewPager) view.findViewById(R.id.english_pager);
        upperImage = (ImageView)view.findViewById(R.id.upper_image);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                English_detail.this.getActivity().onBackPressed();

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      //          ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(English_detail.this.getContext(),sounds1[viewPager.getCurrentItem()]);

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

       /*
       * inilize the Custom adapter
       * and pass getContext it `returns the context view only current running activity.
       * customAdapter is like Beridge
       * **/
        englishCustomPageAdapter = new English_CustomPageAdapter(getContext());

        viewPager.setAdapter(englishCustomPageAdapter);

        viewPager.setCurrentItem(English_Resource.page_number);
      //  ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(this.getActivity().getApplicationContext(),sounds1[English_Resource.page_number]);
        return view;
    }


//called when it will attached to activity
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

 public void btnClicked(View v){


 }
    // called when it deteched with activity
    @Override
    public void onDetach()
    {
        super.onDetach();


         }
}