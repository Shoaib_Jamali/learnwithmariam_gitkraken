package com.appninja.learnwithmariam;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

public class English_container extends AppCompatActivity {

    android.support.v4.app.FragmentTransaction transaction;

@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_english_container);
    getSupportActionBar().hide();
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //creating reference for fragment and inilization
        English_detail frag=new English_detail();

        android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.parent_layout,frag,"Test Fragment");
        transaction.commit();

    }
}
