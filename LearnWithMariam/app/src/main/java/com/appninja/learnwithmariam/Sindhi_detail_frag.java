package com.appninja.learnwithmariam;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {} factory method to
 * create an instance of this fragment.
 */
public class Sindhi_detail_frag extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
   private ViewPager viewPager;
    Sindhi_CustomPagerAdapter sindhi_customPagerAdapter;
    private ImageView back;
 //  int sounds[] = {akh.raw,R.raw.ب,R.raw.ٻ,R.raw.ڀ,R.raw.ت,R.raw.ٿ};

    // TODO: Rename and change types of parameters


    public Sindhi_detail_frag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Sindhi_detail_frag.
     */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_sindhi_detail_frag,container,false);
        viewPager = (ViewPager) view.findViewById(R.id.sindhi_pager);

        sindhi_customPagerAdapter=new Sindhi_CustomPagerAdapter(getContext());
        viewPager.setAdapter(sindhi_customPagerAdapter);

        viewPager.setCurrentItem(Sindhi_Resource.page_number);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //    ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(Sindhi_detail_frag.this.getContext(),Sindhi_sound_resources.sound_upper[viewPager.getCurrentItem()]);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        back = view.findViewById(R.id.sindhi_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Sindhi_detail_frag.this.getActivity().onBackPressed();

            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
           }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
 }
