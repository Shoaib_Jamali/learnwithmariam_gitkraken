package com.appninja.learnwithmariam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Sindhi_Main extends AppCompatActivity {
    private ImageView alif, badk, book, bhitt, tarro, thelo, topi, thoth, sawab, payy, jeem, nymunj, jhirki, jyjung, chechand, chattri;
    private ImageView hajjam, khekhachar, daal, dhedhobi, dedar, dell, dhago, zaal, reel, mari, zanjeer, seen, sheen, sawad, zawad, toto, zoizalim, ainak, gheen;
    private ImageView fee, phee, qalam, kaaf, khee, ghaaf, ghero, ghoro, sing, laam, meem, noon, nywann, wawadho, hee, hamza, yakko,back;
    private Intent intent;

    private int increament;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);
        setContentView(R.layout.activity_sindhi__main);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        MobileAds.initialize(this, "ca-app-pub-2173820520715946~4900274980");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


//        sindhi_main_adview = findViewById(R.id.sindhi_main_adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        sindhi_main_adview.loadAd(adRequest);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_in_left);
               // Sindhi_Main.this.onBackPressed();
                intent=new Intent(getApplication(),Home_activity.class);
                startActivity(intent);

            }
        });
        alif = (ImageView) findViewById(R.id.aliff);
        badk = (ImageView) findViewById(R.id.baay);
        book = (ImageView) findViewById(R.id.bebook);
        bhitt = (ImageView) findViewById(R.id.bhitt);
        tarro = (ImageView) findViewById(R.id.taroo);
        thelo = (ImageView) findViewById(R.id.thello);
        topi = (ImageView) findViewById(R.id.tetopi);
        thoth = (ImageView) findViewById(R.id.thooth);
        sawab = (ImageView) findViewById(R.id.sawab);
        payy = (ImageView) findViewById(R.id.payy);
        jeem = (ImageView) findViewById(R.id.jeem);
        nymunj = (ImageView) findViewById(R.id.nymunj);
        jhirki = (ImageView) findViewById(R.id.jhirki);
        jyjung = (ImageView) findViewById(R.id.jyjung);
        chechand = (ImageView) findViewById(R.id.chechand);
        chattri = (ImageView) findViewById(R.id.chattri);
        hajjam = (ImageView) findViewById(R.id.hajjam);
        khekhachar = (ImageView) findViewById(R.id.khekhachar);
        daal = (ImageView) findViewById(R.id.dedaal);
        dhedhobi = (ImageView) findViewById(R.id.dhedhobi);
        dedar = (ImageView) findViewById(R.id.dedar);
        dell = (ImageView) findViewById(R.id.dedell);
        dhago = (ImageView) findViewById(R.id.dhedhago);
        zaal = (ImageView) findViewById(R.id.zarro);
        reel = (ImageView) findViewById(R.id.re_reel);
        mari = (ImageView) findViewById(R.id.re_mari);
        zanjeer = (ImageView) findViewById(R.id.zanjeer);
        seen = (ImageView) findViewById(R.id.seen);
        sheen = (ImageView) findViewById(R.id.sheen);
        sawad = (ImageView) findViewById(R.id.sawad);

        zawad = (ImageView) findViewById(R.id.zawad);
        toto = (ImageView) findViewById(R.id.toetoto);
        zoizalim = (ImageView) findViewById(R.id.zoizalim);
        ainak = (ImageView) findViewById(R.id.ainak);
        gheen = (ImageView) findViewById(R.id.gheen);
        fee = (ImageView) findViewById(R.id.fee);
        phee = (ImageView) findViewById(R.id.phebheto);
        qalam = (ImageView) findViewById(R.id.qalam);
        kaaf = (ImageView) findViewById(R.id.kalam);
        qalam = (ImageView) findViewById(R.id.qalam);
        khee = (ImageView) findViewById(R.id.khekhatt);
        ghaaf = (ImageView) findViewById(R.id.ghaaf);
        ghoro = (ImageView) findViewById(R.id.ghoro);
        ghero = (ImageView) findViewById(R.id.gheghero);
        sing = (ImageView) findViewById(R.id.sing);
        laam = (ImageView) findViewById(R.id.laam);
        meem = (ImageView) findViewById(R.id.meem);
        noon = (ImageView) findViewById(R.id.noon);
        nywann = (ImageView) findViewById(R.id.nywann);
        wawadho = (ImageView) findViewById(R.id.wawadho);
        hee = (ImageView) findViewById(R.id.hee);
        hamza = (ImageView) findViewById(R.id.hamza);
        yakko = (ImageView) findViewById(R.id.yakko);

        intent=new Intent(getApplication(),Sindhi_container.class);

        alif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Sindhi_Resource.page_number=0;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.akh);
                increament++;
                add_show();

            }
        });


        badk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Sindhi_Main.this.getBaseContext(),"badak",Toast.LENGTH_LONG).show();
                Sindhi_Resource.page_number=1;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.badak);
                increament++;
                add_show();
            }
        });


        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Sindhi_Resource.page_number=2;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.be3);
                increament++;
                add_show();
            }
        });


        bhitt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Sindhi_Resource.page_number=3;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.bhe1);
                increament++;
                add_show();
            }
        });

        tarro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sindhi_Resource.page_number=4;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.tytalwar);
                increament++;
                add_show();

            }
        });


        thelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sindhi_Resource.page_number=5;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.thambho);
                increament++;
                add_show();

            }
        });


        topi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Sindhi_Resource.page_number=6;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.saye4);
                increament++;
                add_show();

            }
        });



        thoth.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=7;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.taa1);
        increament++;
        add_show();

    }
});


        sawab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sindhi_Resource.page_number=8;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.sawab2);
                increament++;
                add_show();
            }
        });
    payy.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=9;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.paa2);
        increament++;
        add_show();
    }
});

    jeem.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=10;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.jeam4);
        increament++;
        add_show();
    }
});

    nymunj.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=11;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ja2);
        increament++;
        add_show();
    }
});

        jhirki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sindhi_Resource.page_number=12;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.jheejhirki);
                increament++;
                add_show();

            }
        });


    jyjung.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=13;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.nyjung);
        increament++;
        add_show();

    }
});




    chechand.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=14;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.cha2);
        increament++;
        add_show();
    }
});


    chattri.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=15;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.chaa2);
        increament++;
        add_show();
    }
});

    hajjam.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=16;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ha4);
        increament++;
        add_show();
    }
});

    khekhachar.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=17;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.kha4);
        increament++;
        add_show();
    }
});

    daal.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=18;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.d111);
        increament++;
        add_show();

    }
});

    dhedhobi.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=19;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.d22222);
            increament++;
            add_show();
        }

    });

    dedar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=20;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.dee4);
            increament++;
            add_show();
        }
    });

    dell.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=21;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.d4);
            increament++;
            add_show();

        }
    });


    dhago.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Sindhi_Resource.page_number=22;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.d11);
            increament++;
            add_show();

        }
    });

    zaal.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=23;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.d2222);
            increament++;
            add_show();
        }
    });

    reel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=24;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ra2);
        increament++;
        add_show();
    }
});

    mari.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=25;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.maari);
        increament++;
        add_show();

    }
});

    zanjeer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=26;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.zanjeer);
            increament++;
            add_show();
        }
    });


    seen.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=27;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.seene2);
            increament++;
            add_show();
        }
    });

    sheen.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=28;
            startActivity(intent);

            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.sheen2);
            increament++;
            add_show();
        }
    });

    sawad.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=29;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.sawat3);
            increament++;
            add_show();

        }
    });

zawad.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=30;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.zawat1);
        increament++;
        add_show();

    }
});

    toto.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=31;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.toeentaraf);
            increament++;
            add_show();
        }
    });

    zoizalim.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Sindhi_Resource.page_number=32;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.zoye1);
            increament++;
            add_show();

        }
    });

    ainak.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Sindhi_Resource.page_number=33;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ain2);
            increament++;
            add_show();


        }
    });

    gheen.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Sindhi_Resource.page_number=34;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ghyan2);
            increament++;
            add_show();


        }
    });
    fee.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=35;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.fa1);
        increament++;
        add_show();

    }
});

    phee.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=36;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.pheeto);
        increament++;
        add_show();


    }
});

    qalam.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=37;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.qa1);
        increament++;
        add_show();


    }
});

    kaaf.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=38;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.k3);
            increament++;
            add_show();

        }
    });

    khee.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=39;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.kaaf2);
            increament++;
            add_show();

        }
    });
    ghaaf.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=40;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ga1);
            increament++;
            add_show();

        }
    });


    ghero.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Sindhi_Resource.page_number=41;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ghaf1);
            increament++;
            add_show();


        }
    });


    ghoro.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=42;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.gha1);
            increament++;
            add_show();


        }
    });

    sing.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=43;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ghaff2);
            increament++;
            add_show();


        }
    });

    laam.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=44;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.laam2);
            increament++;
            add_show();


        }
    });

    meem.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=45;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.maa1);
        increament++;
        add_show();


    }
});

    noon.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=46;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.nun2);
        increament++;
        add_show();

    }
});


    nywann.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=47;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.nata2);
        increament++;
        add_show();


    }
});


    wawadho.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Sindhi_Resource.page_number=48;
            startActivity(intent);
            ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.wao3);
            increament++;
            add_show();

        }
    });

        hee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Sindhi_Resource.page_number=49;
                startActivity(intent);
                ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.haa1);
                increament++;
                add_show();

            }
        });

        hamza.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        Sindhi_Resource.page_number=50;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.hamza1);
        increament++;
        add_show();

    }
});

    yakko.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Sindhi_Resource.page_number=51;
        startActivity(intent);
        ProjectMediaPlayer.getInstance().setupMediaPlayerAndPlay(getApplicationContext(),R.raw.ya3);
        increament++;
        add_show();

    }
});
startZoomInAnimation();

    }
public void add_show(){
        if(increament>=10){
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();

            } else {
                mInterstitialAd = new InterstitialAd(this);
                mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                mInterstitialAd.show();
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            increament=0;

        }


}


    public void startZoomInAnimation() {


        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);



        alif.startAnimation(animation);
        // a.startAnimation(rotate);

        //private ImageView alif, badk, book, bhitt, tarro, thelo, topi, thoth, sawab, payy, jeem, nymunj, jhirki, jyjung, chechand, chattri;
        //private ImageView hajjam, khekhachar, daal, dhedhobi, dedar, dell, dhago, zaal, reel, mari, zanjeer, seen, sheen, sawad, zawad, toto, zoizalim, ainak, gheen;
        //private ImageView fee, phee, qalam, kaaf, khee, ghaaf, ghero, ghoro, sing, laam, meem, noon, nywann, wawadho, hee, hamza, yakko,back;

        badk.startAnimation(animation);
        book.startAnimation(animation);
        bhitt.startAnimation(animation);
        tarro.startAnimation(animation);
        thelo.startAnimation(animation);
        topi.startAnimation(animation);
        thoth.startAnimation(animation);
        sawab.startAnimation(animation);
        ghaaf.startAnimation(animation);

        payy.startAnimation(animation);
        jeem.startAnimation(animation);
        nymunj.startAnimation(animation);
        jhirki.startAnimation(animation);
        jyjung.startAnimation(animation);
        chechand.startAnimation(animation);
        chattri.startAnimation(animation);
        hajjam.startAnimation(animation);
        khekhachar.startAnimation(animation);
        daal.startAnimation(animation);
        dhedhobi.startAnimation(animation);
        dedar.startAnimation(animation);
        dell.startAnimation(animation);
        dhago.startAnimation(animation);
        zaal.startAnimation(animation);
        reel.startAnimation(animation);
        mari.startAnimation(animation);


        zanjeer.startAnimation(animation);
        seen.startAnimation(animation);
        sheen.startAnimation(animation);
        sawad.startAnimation(animation);
        zawad.startAnimation(animation);
        toto.startAnimation(animation);
        zoizalim.startAnimation(animation);


        ainak.startAnimation(animation);
        gheen.startAnimation(animation);
        fee.startAnimation(animation);
        phee.startAnimation(animation);
        qalam.startAnimation(animation);
        kaaf.startAnimation(animation);
        khee.startAnimation(animation);




        ghero.startAnimation(animation);
        ghoro.startAnimation(animation);
        sing.startAnimation(animation);
        laam.startAnimation(animation);
        meem.startAnimation(animation);
        noon.startAnimation(animation);
        nywann.startAnimation(animation);


        wawadho.startAnimation(animation);
        hee.startAnimation(animation);
        hamza.startAnimation(animation);
        yakko.startAnimation(animation);




    }

}
