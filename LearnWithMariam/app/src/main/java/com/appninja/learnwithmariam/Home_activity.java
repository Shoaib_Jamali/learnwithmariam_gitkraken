package com.appninja.learnwithmariam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Home_activity extends AppCompatActivity {
    private Button english,sindhi;
    private Intent intent;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_in_right);

        getSupportActionBar().hide();
        setContentView(R.layout.home_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
// Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        english=(Button)findViewById(R.id.english);
        sindhi=(Button)findViewById(R.id.sindhi);
        ///play=(Button)findViewById(R.id.play);


        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent=new Intent(getApplication(),EnglishMain.class);
                startActivity(intent);

            }
        });

        sindhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent=new Intent(getApplication(),Sindhi_Main.class);
                startActivity(intent);
                   }
        });

//        play.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                intent=new Intent(getApplication(),EnglishMain.class);
//                startActivity(intent);
//
//
//            }
//        });
    }

    }

